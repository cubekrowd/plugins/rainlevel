package net.cubekrowd.rainlevel;

import com.destroystokyo.paper.event.server.ServerTickEndEvent;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.ChannelPromise;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.logging.Level;
import net.minecraft.network.protocol.game.ClientboundGameEventPacket;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.CraftWorld;
import org.bukkit.craftbukkit.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class RainLevelPlugin extends JavaPlugin implements Listener {
    public volatile float configuredRainLevel;
    public String dbFileName = "data.db";

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this);
        loadData();
    }

    @Override
    public void onDisable() {
        dumpData(serialiseData());
    }

    public ByteBuffer serialiseData() {
        // @NOTE(traks) just allocate a big buffer to write to. Should be large
        // enough for our needs
        var res = ByteBuffer.allocate(1 << 20);

        int dataVersion = 0;
        res.putInt(dataVersion);
        res.putFloat(configuredRainLevel);

        return res;
    }

    public void dumpData(ByteBuffer serialised) {
        var dataFolder = getDataFolder();
        if (!dataFolder.exists()) {
            dataFolder.mkdirs();
        }
        var dataFile = new File(dataFolder, dbFileName);
        try {
            Files.write(dataFile.toPath(), Arrays.copyOf(serialised.array(), serialised.position()));
        } catch (IOException e) {
            getLogger().log(Level.SEVERE, "Failed to save data", e);
        }
    }

    public boolean loadData() {
        ByteBuffer buf;
        try {
            var dataFile = new File(getDataFolder(), dbFileName);
            if (!dataFile.exists()) {
                return true;
            }
            buf = ByteBuffer.wrap(Files.readAllBytes(dataFile.toPath()));
        } catch (IOException e) {
            getLogger().log(Level.SEVERE, "Failed to load data", e);
            return false;
        }

        // @TODO(traks) handle exceptions?
        int dataVersion = buf.getInt();
        if (dataVersion == 0) {
            configuredRainLevel = buf.getFloat();
            return true;
        } else {
            getLogger().severe("Unknown database version " + dataVersion);
            return false;
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            sender.sendMessage("Please supply a decimal number between 0 and 1. Enter 0 to disable this plugin. The currently configured rain level is " + configuredRainLevel + ".");
        } else {
            float newValue;
            try {
                newValue = Float.parseFloat(args[0]);
            } catch (NumberFormatException e) {
                sender.sendMessage("That's not a valid decimal number.");
                return true;
            }
            sender.sendMessage("Set the rain level from " + configuredRainLevel + " to " + newValue + ".");
            configuredRainLevel = newValue;
        }
        return true;
    }

    public ChannelPipeline getPlayerPipeline(Player player) {
        return ((CraftPlayer) player).getHandle().connection.connection.channel.pipeline();
    }

    // @NOTE(traks) the goal of this packet handler is to prevent players from
    // getting spammed by rain level change packets every tick, because the
    // server thinks the rain level changes every tick (cause we set it each
    // tick). Probably not gonna save a whole lot of network traffic, but seems
    // better to avoid it if we can
    public class RainPacketHandler extends ChannelOutboundHandlerAdapter {
        public volatile float lastSentRainLevel;

        @Override
        public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
            if (msg instanceof ClientboundGameEventPacket packet) {
                if (packet.getEvent() == ClientboundGameEventPacket.START_RAINING) {
                    lastSentRainLevel = 1;
                } else if (packet.getEvent() == ClientboundGameEventPacket.STOP_RAINING) {
                    lastSentRainLevel = 0;
                } else if (packet.getEvent() == ClientboundGameEventPacket.RAIN_LEVEL_CHANGE) {
                    var config = configuredRainLevel;
                    if (config > 0) {
                        if (Math.abs(config - lastSentRainLevel) < 0.015
                                && Math.abs(config - packet.getParam()) < 0.015) {
                            return;
                        }
                    }
                    lastSentRainLevel = packet.getParam();
                }
            }

            super.write(ctx, msg, promise);
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        var pipeline = getPlayerPipeline(e.getPlayer());
        pipeline.addLast("RainLevel packet handler", new RainPacketHandler());
    }

    // @NOTE(traks) Here's a rough overview of how this all works.
    //
    // Internally, as a weather transition happens, vanilla sends the clients
    // how far the transition has progressed as a floating point number from 0
    // to 1. Here 0 is no rain, and 1 is full rain. It increments (or
    // decrements) the floating point number every tick by 0.01 until it reaches
    // 1 (or 0) and sends those updates to clients.
    //
    // This floating point number is called the rain level. As it increases, it
    // becomes rain-ier and rain/snow becomes more opaque. Here we lock the rain
    // level into place, so rain/snow stays at a fixed level of transparency.
    @EventHandler
    public void onTick(ServerTickEndEvent e) {
        if (configuredRainLevel > 0) {
            for (var world : Bukkit.getWorlds()) {
                var nmsWorld = ((CraftWorld) world).getHandle();
                nmsWorld.rainLevel = configuredRainLevel;
            }
        }
    }
}
