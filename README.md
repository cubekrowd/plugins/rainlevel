# RainLevel

This plugin allows you to lock the weather transition from no rain to full rain into place. This way you can for example make it snow with more transparent snow particles.

There is only a single command `/rainlevel` with permission `rainlevel.admin`.
It allows you to set the stage of the weather transition as a decimal number between 0 and 1. Here 0 means no rain and 1 means full rain. Effectively, this allows you to control how transparent rain and snow particles are.
